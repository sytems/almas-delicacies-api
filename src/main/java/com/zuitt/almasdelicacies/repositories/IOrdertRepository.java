package com.zuitt.almasdelicacies.repositories;

import com.zuitt.almasdelicacies.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface IOrdertRepository extends JpaRepository<Order, Integer> {
    @Query(value = "SELECT * FROM orders WHERE course_id = ?1 AND user_id = ?2 LIMIT 1", nativeQuery = true)
    Order findPriorEnrollment(int courseId, int userId);
}
