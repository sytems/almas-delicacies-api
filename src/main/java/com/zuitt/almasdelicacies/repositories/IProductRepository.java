package com.zuitt.almasdelicacies.repositories;

import com.zuitt.almasdelicacies.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IProductRepository extends JpaRepository<Product, Integer> {
    @Query(value = "SELECT * FROM products WHERE is_active = 1", nativeQuery = true)
    List<Product> findAllActive();
}
