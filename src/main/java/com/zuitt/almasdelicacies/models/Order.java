package com.zuitt.almasdelicacies.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int productId;
    private int userId;
    @Column(insertable = false)
    private LocalDateTime datetimeOrdered;

    public Order() { }

    public Order(
        int productId,
        int userId
    ) {
        this.productId = productId;
        this.userId = userId;
        this.datetimeOrdered = LocalDateTime.now();
    }

    public int getId() {
        return this.id;
    }

    public int getProductId() {
        return this.productId;
    }

    public int getUserId() {
        return this.userId;
    }

    public LocalDateTime getDatetimeOrdered() {
        return this.datetimeOrdered;
    }
}
