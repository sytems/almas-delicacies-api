package com.zuitt.almasdelicacies.controllers;

import com.zuitt.almasdelicacies.models.Product;
import com.zuitt.almasdelicacies.repositories.IProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
public class ProductController extends AppController {
    @Autowired
    private IProductRepository product;

    @GetMapping("/api/products")
    public List<Product> get() {
        return product.findAllActive();
    }

    @GetMapping("/api/products/{id}")
    public Product getOne(@PathVariable int id) {
        return product.findById(id).get();
    }

    @PostMapping("/api/products")
    public ResponseEntity<Object> add(@RequestBody Product newProduct) {
        HashMap<String, String> response = new HashMap<>();


        product.save(new Product(
                newProduct.getName(),
                newProduct.getDescription(),
                newProduct.getPrice()
        ));

        response.put("result", "added");

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("/api/products/{id}")
    public ResponseEntity<Object> update(@PathVariable int id, @RequestBody HashMap<String, Object> data) {
        HashMap<String, String> response = new HashMap<>();
        Product selectedCourse = product.findById(id).get();

        selectedCourse.setName(data.get("name").toString());
        selectedCourse.setDescription(data.get("description").toString());
        selectedCourse.setPrice(Double.parseDouble(data.get("price").toString()));

        product.save(selectedCourse);
        response.put("result", "updated");

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/api/products/{id}")
    public ResponseEntity<Object> archive(@PathVariable int id) {
        HashMap<String, String> response = new HashMap<>();
        Product selectedCourse = product.findById(id).get();

        selectedCourse.setIsActive(false);

        product.save(selectedCourse);
        response.put("result", "archived");

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}